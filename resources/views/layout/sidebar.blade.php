<div class="mdk-drawer js-mdk-drawer" id="default-drawer">
	<div class="mdk-drawer__content">
		<div class="sidebar sidebar-dark sidebar-left" data-perfect-scrollbar>



			<a href="/" class="sidebar-brand ">
				<!-- <img class="sidebar-brand-icon" src="assets/images/logo/accent-teal-100@2x.png" alt="Huma"> -->
				<span>Taman Sari</span>
			</a>

			<div class="sidebar-account mx-16pt mb-16pt dropdown">
				<a href="#" class="nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown"
					data-caret="false">
					<img width="32" height="32" class="rounded-circle mr-8pt"
						src="{{ asset('huma/images/people/50/guy-3.jpg') }}" alt="account" />
					<span class="flex d-flex flex-column mr-8pt">
						<span class="text-black-100">Laza Bogdan</span>
						<small class="text-black-50">Administrator</small>
					</span>
					<i class="material-icons text-black-20 icon-16pt">keyboard_arrow_down</i>
				</a>
				<div class="dropdown-menu dropdown-menu-full dropdown-menu-caret-center">
					<div class="dropdown-header"><strong>Account</strong></div>
					<a class="dropdown-item" href="edit-account.html">Edit Profile</a>
					<a class="dropdown-item" href="login.html">Logout</a>
				</div>
			</div>

			<div class="sidebar-heading">Menu</div>
			<ul class="sidebar-menu">
				<li class="sidebar-menu-item active">
					<a class="sidebar-menu-button" href="/">
						<span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">dashboard</span>
						<span class="sidebar-menu-text">Dashboard</span>
					</a>
				</li>
				<li class="sidebar-menu-item">
					<a class="sidebar-menu-button" data-toggle="collapse" href="#profil_menu">
						<span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">settings</span>
						Profil Desa
						<span class="ml-auto sidebar-menu-toggle-icon"></span>
					</a>
					<ul class="sidebar-submenu collapse sm-indent" id="profil_menu">
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/profil/perangkat">
								<span class="sidebar-menu-text">Perangkat Desa Gentan</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/profil/bpd">
								<span class="sidebar-menu-text">BPD Desa Gentan</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/profil/rtrw">
								<span class="sidebar-menu-text">RT RW Desa Gentan</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/profil/pkk">
								<span class="sidebar-menu-text">PKK Desa Gentan</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="sidebar-menu-item">
					<a class="sidebar-menu-button" data-toggle="collapse" href="#administrasi">
						<span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">description</span>
						Administrasi
						<span class="ml-auto sidebar-menu-toggle-icon"></span>
					</a>
					<ul class="sidebar-submenu collapse sm-indent" id="administrasi">
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/administrasi/pengantar">
								<span class="sidebar-menu-text">Surat Pengantar</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/administrasi/masuk">
								<span class="sidebar-menu-text">Surat Masuk</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/administrasi/keluar">
								<span class="sidebar-menu-text">Surat Keluar</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/administrasi/laporan">
								<span class="sidebar-menu-text">Laporan Administrasi</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="sidebar-menu-item">
					<a class="sidebar-menu-button" data-toggle="collapse" href="#aset">
						<span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">house</span>
						Aset Desa
						<span class="ml-auto sidebar-menu-toggle-icon"></span>
					</a>
					<ul class="sidebar-submenu collapse sm-indent" id="aset">
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/aset/data">
								<span class="sidebar-menu-text">Data Aset Desa</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/aset/tambah">
								<span class="sidebar-menu-text">Tambah Data</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/aset/laporan">
								<span class="sidebar-menu-text">Laporan Aset</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="sidebar-menu-item">
					<a class="sidebar-menu-button" data-toggle="collapse" href="#kependudukan">
						<span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">folder</span>
						Kependudukan
						<span class="ml-auto sidebar-menu-toggle-icon"></span>
					</a>
					<ul class="sidebar-submenu collapse sm-indent" id="kependudukan">
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/kependudukan/data">
								<span class="sidebar-menu-text">Data Penduduk</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/kependudukan/kelahiran">
								<span class="sidebar-menu-text">Kelahiran</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/kependudukan/kematian">
								<span class="sidebar-menu-text">Kematian</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/kependudukan/datang">
								<span class="sidebar-menu-text">Pindah Datang</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/kependudukan/pergi">
								<span class="sidebar-menu-text">Pindah Pergi</span>
							</a>
						</li>
					</ul>
				</li>

				<li class="sidebar-menu-item">
					<a class="sidebar-menu-button" data-toggle="collapse" href="#berkas">
						<span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">supervisor_account</span>
						Berkas Nikah
						<span class="ml-auto sidebar-menu-toggle-icon"></span>
					</a>
					<ul class="sidebar-submenu collapse sm-indent" id="berkas">
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/nikah/daftar">
								<span class="sidebar-menu-text">Daftar Nikah</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/nikah/pengantar">
								<span class="sidebar-menu-text">Pengantar Nikah</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/nikah/cetak">
								<span class="sidebar-menu-text">Cetak Dokumen</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="sidebar-menu-item">
					<a class="sidebar-menu-button" data-toggle="collapse" href="#pengaturan">
						<span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">build</span>
						Pengaturan
						<span class="ml-auto sidebar-menu-toggle-icon"></span>
					</a>
					<ul class="sidebar-submenu collapse sm-indent" id="pengaturan">
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/pengaturan/user">
								<span class="sidebar-menu-text">User</span>
							</a>
						</li>
						<li class="sidebar-menu-item">
							<a class="sidebar-menu-button" href="/pengaturan/ubahpassword">
								<span class="sidebar-menu-text">Ubah Passwords</span>
							</a>
						</li>
					</ul>
				</li>




			</ul>




		</div>
	</div>
</div>