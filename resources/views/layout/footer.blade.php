<div class="js-fix-footer footer border-top-2">
    <div class="">
        <div class="container-fluid page__container">
            <div class="rounded page-section py-lg-32pt px-16pt " style="padding: 0;">
                <div class="row">
                    <div class="col text-md-right">
                        <p class="text-black-50 small mb-0">Copyright 2023 &copy; All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>