<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="{{ asset('huma/images/') }}" type="image/x-icon" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('title') - Taman Sari</title>
	<!-- Main Styles -->
	<link
		href="https://fonts.googleapis.com/css?family=Lato:400,700%7COswald:300,400,500,700%7CRoboto:400,500%7CExo+2:600&display=swap"
		rel="stylesheet">

	<!-- Perfect Scrollbar -->
	<link type="text/css" href="{{ asset('huma/vendor/perfect-scrollbar.css') }} " rel="stylesheet">

	<!-- Material Design Icons -->
	<link type="text/css" href="{{ asset('huma/css/material-icons.css') }}" rel="stylesheet">

	<!-- Font Awesome Icons -->
	<link type="text/css" href="{{ asset('huma/css/fontawesome.css') }}" rel="stylesheet">

	<!-- Preloader -->
	<link type="text/css" href="{{ asset('huma/vendor/spinkit.css') }}" rel="stylesheet">
	<link type="text/css" href="{{ asset('huma/css/preloader.css') }}" rel="stylesheet">

	<!-- App CSS -->
	<link type="text/css" href="{{ asset('huma/css/app.css') }}" rel="stylesheet">

	<!-- Dark Mode CSS (optional) -->
	<link type="text/css" href="{{ asset('huma/css/dark-mode.css') }}" rel="stylesheet">

	<!-- Vector Maps -->
	<link type="text/css" href="{{ asset('huma/vendor/jqvmap/jqvmap.min.css') }}" rel="stylesheet">

	<!-- Custom CSS -->
	<link type="text/css" href="{{ asset('huma/css/custom.css') }}" rel="stylesheet">

	<!-- Flatpickr -->
	<link type="text/css" href="assets/css/flatpickr.css" rel="stylesheet">
	<link type="text/css" href="assets/css/flatpickr-airbnb.css" rel="stylesheet">

	<!-- DateRangePicker -->
	<link type="text/css" href="assets/vendor/daterangepicker.css" rel="stylesheet">

	<!-- Quill Theme -->
	<link type="text/css" href="assets/css/quill.css" rel="stylesheet">

	<!-- Touchspin -->
	<link type="text/css" href="assets/css/bootstrap-touchspin.css" rel="stylesheet">

	<!-- Select2 -->
	<link type="text/css" href="assets/vendor/select2/select2.min.css" rel="stylesheet">
	<link type="text/css" href="assets/css/select2.css" rel="stylesheet">

	<style>
		.app-settings-button {
			top: 120px !important;
		}
	</style>

</head>



<body class="layout-app layout-sticky-subnav ">

	<div class="mdk-drawer-layout js-mdk-drawer-layout" data-push data-responsive-width="992px">
		<div class="mdk-drawer-layout__content page-content">

			<!-- Header -->



			<!-- // END Header -->

			<!-- /.main-menu -->
			@include('layout.header')
			<!-- /.fixed-navbar -->

			<div class="container-fluid page__container">
				@yield('container')
			</div>


			@include('layout.footer')

		</div>
		<!-- // END drawer-layout__content -->

		<!-- drawer -->

		@include('layout.sidebar')
		<!-- // END drawer -->
	</div>
	<!-- // END drawer-layout -->

	<!-- App Settings FAB -->
	<div id="app-settings">
		<app-settings layout-active="app" :layout-location="{
			'compact': 'compact-index.html',
			'mini': 'mini-index.html',
			'app': 'index.html',
			'boxed': 'boxed-index.html',
			'sticky': 'sticky-index.html',
			'default': 'fixed-index.html'
			}" sidebar-type="light" sidebar-variant="bg-body">
		</app-settings>
	</div>
	<!-- jQuery -->
	<script src="{{ asset('huma/vendor/jquery.min.js') }}"></script>

	<!-- Bootstrap -->
	<script src="{{ asset('huma/vendor/popper.min.js') }}"></script>
	<script src="{{ asset('huma/vendor/bootstrap.min.js') }}"></script>

	<!-- Perfect Scrollbar -->
	<script src="{{ asset('huma/vendor/perfect-scrollbar.min.js') }}"></script>

	<!-- DOM Factory -->
	<script src="{{ asset('huma/vendor/dom-factory.js') }}"></script>

	<!-- MDK -->
	<script src="{{ asset('huma/vendor/material-design-kit.js') }}"></script>

	<!-- App JS -->
	<script src="{{ asset('huma/js/app.js') }}"></script>

	<!-- Highlight.js -->
	<script src="{{ asset('huma/js/hljs.js') }}"></script>

	<!-- Global Settings -->
	<script src="{{ asset('huma/js/settings.js') }}"></script>

	<!-- Flatpickr -->
	<script src="{{ asset('huma/vendor/flatpickr/flatpickr.min.js') }}"></script>
	<script src="{{ asset('huma/js/flatpickr.js') }}"></script>

	<!-- Moment.js -->
	<script src="{{ asset('huma/vendor/moment.min.js') }}"></script>
	<script src="{{ asset('huma/vendor/moment-range.js') }}"></script>

	<!-- Chart.js -->
	<script src="{{ asset('huma/vendor/Chart.min.js') }}"></script>
	<script src="{{ asset('huma/js/chartjs.js') }}"></script>

	<!-- Chart.js Samples -->
	<script src="{{ asset('huma/js/page.analytics-dashboard.js') }}"></script>

	<!-- Vector Maps -->
	<script src="{{ asset('huma/vendor/jqvmap/jquery.vmap.min.js') }}"></script>
	<script src="{{ asset('huma/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
	<script src="{{ asset('huma/js/vector-maps.js') }}"></script>

	<!-- List.js -->
	<script src="{{ asset('huma/vendor/list.min.js') }}"></script>
	<script src="{{ asset('huma/js/list.js') }}"></script>

	<!-- Tables -->
	<script src="{{ asset('huma/js/toggle-check-all.js') }}"></script>
	<script src="{{ asset('huma/js/check-selected-row.js') }}"></script>

	<!-- App Settings (safe to remove) -->
	<script src="{{ asset('huma/js/app-settings.js') }}"></script>

	<!-- DateRangePicker -->
	<script src="assets/vendor/moment.min.js"></script>
	<script src="assets/vendor/daterangepicker.js"></script>
	<script src="assets/js/daterangepicker.js"></script>




</body>

</html>