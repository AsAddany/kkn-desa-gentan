@extends('layout.app')
@section('title', 'Dashboard')
@section('container')


<div class="page-separator mt-4">
    <div class="page-separator__text">Tambah Aset Desa</div>
</div>

<div class="row mb-32pt">
    <div class="col-lg-8 d-flex align-items-center">
        <div class="flex" style="max-width: 100%">
            <div class="form-group">
                <label class="form-label" for="exampleInputEmail1">Nama Barang</label>
                <input type="email" class="form-control" id="exampleInputEmail1">
            </div>
            <div class="form-group">
                <label class="form-label" for="exampleInputEmail1">Kode Barang</label>
                <input type="email" class="form-control" id="exampleInputEmail1">
            </div>

            <div class="form-group">
                <label class="form-label">Tanggal Berlaku</label>
                <input id="flatpickrSample01" type="hidden" class="form-control flatpickr-input" data-toggle="flatpickr"
                    value="today">
            </div>
            <div class="form-group">
                <label class="form-label" for="exampleInputEmail1">Keterangan</label>
                <input type="email" class="form-control" id="exampleInputEmail1">
            </div>

            <div class="my-3">
                <button type="button" class="btn btn-primary">
                    Tambah Aset
                </button>
            </div>

        </div>
    </div>
</div>


@endsection