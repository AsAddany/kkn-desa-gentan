@extends('layout.app')
@section('title', 'Dashboard')
@section('container')
<div class="container-fluid page__container">
    <form action="sticky-edit-account.html">
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Edit User Profile</h4>
                    <div class="list-group list-group-form">
                        <div class="list-group-item">
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-3">Your photo</label>
                                <div class="col-sm-9 media align-items-center">
                                    <a href="" class="media-left mr-16pt">
                                        <img src="{{ asset('huma/images/people/110/guy-3.jpg') }}" alt="people"
                                            width="56" class="rounded-circle" />
                                    </a>
                                    <div class="media-body">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="form-group row align-items-center mb-0">
                                <label class="form-label col-form-label col-sm-3">First name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" value="Alexander"
                                        placeholder="Your first name ...">
                                </div>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="form-group row align-items-center mb-0">
                                <label class="form-label col-form-label col-sm-3">Last name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" value="Watson"
                                        placeholder="Your last name ...">
                                </div>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="form-group row align-items-center mb-0">
                                <label class="form-label col-form-label col-sm-3">Email address</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" value="alexander.watson@fake-mail.com"
                                        placeholder="Your email address ...">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
@endsection