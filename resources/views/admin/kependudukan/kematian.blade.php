@extends('layout.app')
@section('title', 'Dashboard')
@section('container')
<div class="page-separator mt-4">
    <div class="page-separator__text">Kematian</div>
</div>

<div class="my-3">
    <button type="button" class="btn btn-primary">
        <i class="material-icons icon--left">add</i> Tambah
    </button>
</div>

<div class="card mb-lg-32pt">

    <div class="table-responsive" data-toggle="lists" data-lists-values='["js-lists-values-name"]'>

        <table class="table table-bordered table-flush mb-0 thead-border-top-0 table-nowrap">
            <thead>
                <tr>
                    <th>
                        <a href="javascript:void(0)" class="sort" data-sort="js-lists-values-earnings">No.</a>
                    </th>
                    <th>
                        <a href="javascript:void(0)" class="sort" data-sort="js-lists-values-earnings">NAMA</a>
                    </th>
                    <th>
                        <a href="javascript:void(0)" class="sort" data-sort="js-lists-values-earnings">NIK</a>
                    </th>
                    <th style>
                        <a href="javascript:void(0)" class="sort" data-sort="js-lists-values-earnings">ALAMAT</a>
                    </th>
                    <th>
                        <a href="javascript:void(0)" class="sort" data-sort="js-lists-values-earnings">JENIS KELAMIN</a>
                    </th>
                    <th>
                        <a href="javascript:void(0)" class="sort" data-sort="js-lists-values-earnings">AKSI</a>
                    </th>
                </tr>
            </thead>
            <tbody class="list" id="contacts">

                <tr>
                    <td>1</td>
                    <td>SUMARNI</td>
                    <td>3311066302660001</td>
                    <td>Kartasura</td>
                    <td>Perempuan</td>
                    <td>
                        <div class="button-list">
                            <button type="button" class="btn btn-warning">
                                <i class="material-icons">edit</i>
                            </button>
                            <button type="button" class="btn btn-danger">
                                <i class="material-icons">delete</i>
                            </button>
                        </div>
                    </td>
                </tr>


            </tbody>
        </table>
    </div>

    <div class="card-footer border-0 p-8pt">

        <ul class="pagination justify-content-start pagination-xsm m-0">
            <li class="page-item disabled">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true" class="material-icons">chevron_left</span>
                    <span>Prev</span>
                </a>
            </li>
            <li class="page-item dropdown">
                <a class="page-link dropdown-toggle" data-toggle="dropdown" href="#" aria-label="Page">
                    <span>1</span>
                </a>
                <div class="dropdown-menu">
                    <a href="" class="dropdown-item active">1</a>
                    <a href="" class="dropdown-item">2</a>
                    <a href="" class="dropdown-item">3</a>
                    <a href="" class="dropdown-item">4</a>
                    <a href="" class="dropdown-item">5</a>
                </div>
            </li>
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span>Next</span>
                    <span aria-hidden="true" class="material-icons">chevron_right</span>
                </a>
            </li>
        </ul>

    </div>
    <!-- <div class="card-body text-right">
15 <span class="text-50">of 1,430</span> <a href="#" class="text-50"><i class="material-icons ml-1">arrow_forward</i></a>
</div> -->

</div>
@endsection