<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.index');
});

Route::get('/login', function () {
    return view('auth.login');
});

Route::prefix('profil')->group(function () {
    Route::get('/bpd', function () {
        return view('admin.profil.bpd');
    });
    
    Route::get('/perangkat', function () {
        return view('admin.profil.perangkat');
    });
    
    Route::get('/pkk', function () {
        return view('admin.profil.pkk');
    });
    
    Route::get('/rtrw', function () {
        return view('admin.profil.rtrw');
    });
});

Route::prefix('administrasi')->group(function () {
    Route::get('/pengantar', function () {
        return view('admin.administrasi.pengantar');
    });
    
    Route::get('/masuk', function () {
        return view('admin.administrasi.masuk');
    });
    
    Route::get('/keluar', function () {
        return view('admin.administrasi.keluar');
    });
    
    Route::get('/laporan', function () {
        return view('admin.administrasi.laporan');
    });
});

Route::prefix('aset')->group(function () {
    Route::get('/data', function () {
        return view('admin.aset.data');
    });
    
    Route::get('/tambah', function () {
        return view('admin.aset.tambah');
    });
    
    Route::get('/laporan', function () {
        return view('admin.aset.laporan');
    });
    
});

Route::prefix('kependudukan')->group(function () {
    Route::get('/data', function () {
        return view('admin.kependudukan.data');
    });
    
    Route::get('/kelahiran', function () {
        return view('admin.kependudukan.kelahiran');
    });
    
    Route::get('/kematian', function () {
        return view('admin.kependudukan.kematian');
    });
    
    Route::get('/datang', function () {
        return view('admin.kependudukan.datang');
    });
    
    Route::get('/pergi', function () {
        return view('admin.kependudukan.pergi');
    });
    
});

Route::prefix('nikah')->group(function () {
    Route::get('/daftar', function () {
        return view('admin.nikah.daftar');
    });
    
    Route::get('/pengantar', function () {
        return view('admin.nikah.pengantar');
    });
    
    Route::get('/cetak', function () {
        return view('admin.nikah.cetak');
    });
    
});

Route::prefix('pengaturan')->group(function () {
    Route::get('/user', function () {
        return view('admin.pengaturan.user');
    });
    
    Route::get('/ubahpassword', function () {
        return view('admin.pengaturan.ubahpassword');
    });
    
});


